import { useState } from "react";
import { useTaskContext } from "../../contexts/TaskContext";

export const TaskList = () => {
  const taskContext = useTaskContext();
  const [filter, setFilter] = useState(false);

  return (
    <div className="section">
      <h3>Task List</h3>
      <div>
        <input
          type="checkbox"
          name="filter"
          id="filter"
          onChange={() => setFilter(!filter)}
          checked={filter}
        />
        <label htmlFor="filter">Hide Completed</label>
      </div>

      {taskContext.tasks
        .filter((t) => (filter ? !t.completed : true))
        .map((task) => (
          <button
            onClick={() => taskContext.toggle(task.id.toNumber())}
            className="task"
            key={"task" + task.id.toNumber()}
          >
            <div>
              <input
                onChange={() => taskContext.toggle(task.id.toNumber())}
                checked={task.completed}
                type="checkbox"
                name="completed"
              />
            </div>
            <p>id: {task.id.toNumber()}</p>
            <p>content: {task.content}</p>
            <p>completed:{task.completed}</p>
          </button>
        ))}
    </div>
  );
};
