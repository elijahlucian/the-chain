import { useState } from "react";
import { useTaskContext } from "../../contexts/TaskContext";

export const CreateTask = () => {
  const taskContext = useTaskContext();
  const [task, setTask] = useState("");

  const submit = () => {
    if (task.length < 5) return;

    taskContext.create(task);

    // create task
    console.log("Creating Task", task);
    setTask("");
  };

  return (
    <div className="section form">
      <h3>New Task:</h3>
      <input
        onChange={(v) => setTask(v.target.value)}
        value={task}
        onKeyDown={(e) => e.key === "Enter" && submit()}
      ></input>
    </div>
  );
};
