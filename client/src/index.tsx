import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { App } from "./App";
import reportWebVitals from "./reportWebVitals";
import { Web3ContextProvider } from "./contexts/web3provider";
import { TaskContextProvider } from "./contexts/TaskContext";

ReactDOM.render(
  <React.StrictMode>
    <Web3ContextProvider>
      <TaskContextProvider>
        <App />
      </TaskContextProvider>
    </Web3ContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
