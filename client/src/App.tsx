import "./App.css";
import { CreateTask } from "./pages/AddTask";
import { TaskList } from "./pages/TaskList";
import { AccountInfo } from "./partials/AccountInfo";

export const App = () => {
  return (
    <div className="app">
      <div className="header">
        <h1>Janky Blockchain Todo List</h1>
        <AccountInfo />
      </div>
      <div className="body">
        <CreateTask />
        <TaskList />
      </div>
      <div className="footer">
        <p>made by E$Money</p>
      </div>
    </div>
  );
};
