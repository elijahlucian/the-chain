import { useEffect, useRef, useState } from "react";
import { useWeb3Context } from "../contexts/web3provider";

export const AccountInfo = () => {
  const ctx = useRef(useWeb3Context());

  const [accountNumber, setAccountNumber] = useState("");

  useEffect(() => {
    const get = async () => {
      setAccountNumber(await ctx.current.getAccount());
    };

    get();
  }, []);

  return <div>contract: {accountNumber}</div>;
};
