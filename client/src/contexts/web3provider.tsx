import {
  createContext,
  ReactChild,
  useContext,
  useEffect,
  useState,
} from "react";
import Web3 from "web3";
import { initWeb3 } from "../web3";
import contract from "@truffle/contract";

type Web3Context = {
  web3: Web3 | null;
  connect: () => void;
  disconnect: () => void;
  getAccount: () => Promise<string>;
  loadContract: (json: any) => any;
};

const Context = createContext<Web3Context | null>(null);

type Props = {
  children: ReactChild;
};

export const Web3ContextProvider = ({ children }: Props) => {
  const [web3, setWeb3] = useState<Web3 | null>(null);
  const [account, setAccount] = useState<string>("");

  useEffect(() => {
    const load = async () => {
      const web3 = await initWeb3();
      if (!web3) throw new Error("shit, son!");

      web3.setProvider(
        new Web3.providers.HttpProvider("HTTP://127.0.0.1:7545")
      );

      setWeb3(web3);
    };

    load();
  }, []);

  useEffect(() => {
    const init = async () => {
      if (!web3) return;

      web3.eth.defaultAccount = (await web3?.eth.getAccounts())[0];
      console.log(web3.eth.defaultAccount);
    };

    init();
  }, [web3]);

  const connect = () => {
    // connect to wallet
  };
  const disconnect = () => {
    // disconnect from wallet
  };

  const getAccount = async () => {
    if (!web3) throw new Error("should be logged in by now");
    return (await web3?.eth.getAccounts())[0];
  };

  const loadContract = (json: any) => {
    if (!web3) throw new Error("should be logged in by now");
    // @ts-ignore
    const truffleContract = contract(json);
    truffleContract.setProvider(web3.currentProvider);
    return truffleContract;
  };

  return (
    <Context.Provider
      value={{ connect, disconnect, getAccount, loadContract, web3 }}
    >
      {web3 && children}
    </Context.Provider>
  );
};

export const useWeb3Context = () => {
  const ctx = useContext(Context);
  if (!ctx) throw new Error();

  return ctx;
};
