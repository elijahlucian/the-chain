import React, {
  createContext,
  useContext,
  useState,
  useEffect,
  useRef,
  useCallback,
} from "react";
import { useWeb3Context } from "./web3provider";
import { mapI } from "@dank-inc/lewps";
import { Task } from "../types";

import schema from "../contracts/TodoList.json";

type Props = {
  children: React.ReactNode;
};
type Context = {
  fetch: () => Promise<void>;
  create: (content: string) => Promise<void>;
  toggle: (id: number) => Promise<void>;
  tasks: Task[];
};

// Just find-replace "TaskContext" with whatever context name you like. (ie. DankContext)
const TaskContext = createContext<Context | null>(null);

export const TaskContextProvider = ({ children }: Props) => {
  const web3Context = useRef(useWeb3Context());
  const [contract, setContract] = useState<any>(null);
  const [tasks, setTasks] = useState<Task[]>([]);

  const fetch = useCallback(async () => {
    console.log("fetching tasks");
    if (!contract) return;

    const taskCount = await contract.taskCount();
    const tasks = await Promise.all(
      mapI(taskCount, async (i) => contract.tasks(i + 1))
    );

    console.log(tasks);
    setTasks(tasks);
  }, [contract]);

  useEffect(() => {
    const init = async () => {
      const contract = web3Context.current.loadContract(schema);
      setContract(await contract.deployed());
    };
    if (!contract) init();
    else fetch();
  }, [contract, fetch]);

  const create = async (content: string) => {
    try {
      console.log(await web3Context.current.getAccount());

      const task = await contract.createTask(content, {
        from: await web3Context.current.getAccount(),
      });

      console.log("created task", task);

      fetch();
    } catch (err) {
      console.log(err);
    }
  };

  const toggle = async (id: number) => {
    // mark task as complete
    try {
      await contract.toggle(id, {
        from: await web3Context.current.getAccount(),
      });
      fetch();
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <TaskContext.Provider value={{ fetch, create, toggle, tasks }}>
      {contract && children}
    </TaskContext.Provider>
  );
};

export const useTaskContext = () => {
  const context = useContext(TaskContext);

  if (!context)
    throw new Error(
      "TaskContext must be called from within the TaskContextProvider"
    );

  return context;
};
