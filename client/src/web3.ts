import Web3 from "web3";

declare global {
  interface Window {
    ethereum?: any;
    web3?: Web3;
  }
}

export const initWeb3 = async () => {
  if (!window.ethereum) return null;
  if (!window.web3) return null;

  const web3 = new Web3();

  // await window.ethereum.enable();

  return web3;
};
